=== Taxonomy Index Page For Genesis ===
Requires at least: 4.4
Tested up to: 4.4

This create an index of the selected custom taxonomy terms.

Requires the Genesis Framework

== Changelog ==

= 1.0.0 =
Initial build