<?php
/**
 * Represents the view for the administration dashboard.
 *
 * @package  CW_Taxonomy_Index_Page_For_Genesis
 *
 */
?>
<div class="wrap">

	<?php screen_icon(); ?>
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<form method="post" action="options.php">

		<?php settings_fields( 'cw-taxonomy-index-page-for-genesis-group' ); ?>

		<label for="taxonomy"><?php echo _e( 'Select which custom taxonomy create the index from', $this->plugin_slug ); ?></label><br />
		<select id="taxonomy" name="taxonomy">
			<?php

			$args = array(
			  'public'   => true,
		  	);
			$output = 'objects';
			$taxonomies = get_taxonomies( $args, $output );
			foreach( $taxonomies as $taxonomy ) {
				
				if( esc_attr( get_option('taxonomy') ) == $taxonomy->name ){
					$selected = "selected";
				}
				else{
					$selected = "";
				}

				$option = '<option ' . $selected . ' value="'. esc_attr( $taxonomy->name ) .'">';
				$option .= esc_html( $taxonomy->labels->name );
				$option .= '</option>';
				echo $option;
			}

			?>
		</select><br><br>
		
		<label for="index_page"><?php echo _e( 'Select which page to add this index to', $this->plugin_slug  );?></label><br>
		<select id="index_page" name="index_page">
			<?php
			$pages = get_pages();
			foreach ( $pages as $page ) {
				if( esc_attr( get_option('index_page') ) == $page->ID ){
					$selected = "selected";
				}
				else {
					$selected = "";
				}
				$option = '<option ' . $selected . ' value="' . esc_attr( $page->ID ) . '">';
				$option .= esc_html( $page->post_title );
				$option .= '</option>';
				echo $option;
			}
			?>
		</select><br><br>

		<label for="ncols"><?php echo _e( 'Select how many columns', $this->plugin_slug  );?></label><br>
		<select id="ncols" name="ncols">
			<?php
			for( $i = 1; $i < 5; $i++ ) {
				if( esc_attr( get_option('ncols') ) == $i ) {
					$selected = "selected";
				}
				else {
					$selected = "";
				}
				$option = '<option ' . $selected . ' value="' . esc_attr( $i ) . '">';
				$option .= esc_html( $i );
				$option .= '</option>';
				echo $option;
			}
			?>
		</select><br><br>

		<label for="show_post_count"><?php echo _e( 'Show post count?', $this->plugin_slug  );?></label><br>
		<select id="show_post_count" name="show_post_count">
			<?php

			$show_post_count_options = array( 'Yes', 'No' );

			foreach( $show_post_count_options as $show_count ) {
				if( esc_attr( get_option( 'show_post_count' ) ) == $show_count ) {
					$selected = "selected";
				}
				else {
					$selected = "";
				}
				$option = '<option ' . $selected . ' value="' .  $show_count . '">';
				$option .= esc_html( $show_count );
				$option .= '</option>';
				echo $option;
			}
			?>
		</select>

		<?php submit_button(); ?>

	</form>

</div>
