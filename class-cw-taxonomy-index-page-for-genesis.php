<?php
/**
 * Taxonomy Index Page For Genesis
 *
 * @package   CW_Custom_Taxonomy_Index_Page_For_Genesis
 */

class cw_taxonomy_index_page_for_genesis {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @const   string
	 */
	const VERSION = '1.0.0';

	/**
	 * Unique identifier for the plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'cw-taxonomy-index-page-for-genesis';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Add the options page and menu item.
	    add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

	    // Register option page settings
	    add_action( 'admin_init', array( $this, 'register_plugin_settings' ) );

		// Add an action link pointing to the options page.
		$plugin_basename = plugin_basename( plugin_dir_path( __FILE__ ) . 'cw-taxonomy-index-page-for-genesis.php' );
		add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'add_action_links' ) );

        // Check if index should be added to page
        add_action( 'genesis_entry_content', array( $this, 'add_index_after_entry'), 15 );

        // Add Some CSS
        add_action( 'wp_enqueue_scripts', array( $this, 'load_styles' ) );

	}

	/**
	 * Check if we should add the index
	 *
	 * @since     1.0.0
	 *
	 */
	public function add_index_after_entry() {

		// Get values from options
		$index_page = (int) get_option( 'index_page' );
		$tax = get_option( 'taxonomy' );
		$ncols = (int) get_option( 'ncols' );
		$show_post_count = get_option( 'show_post_count' );
		$show_count = false;
		if( 'Yes' == $show_post_count ) {
			$show_count = true;
		}

		// column classes
		$column_class = array( '', '', 'one-half', 'one-third', 'one-fourth' );
		$li_class = '';

		// if we're not on our selected page bail
		if( !is_page( $index_page ) ) {
			return;
		}

		// Get all taxonomy terms
		$args = array(
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => true,
			'fields' => 'all',
		);

		$terms = get_terms( $tax, $args );

		// if terms is empty or error then bail
		if( empty( $terms ) || is_wp_error( $terms ) ) {
			return;
		}
		
		// Loop through terms to find out letters in use and number of terms with that letter
		$prev_letter = strtoupper( $terms[0]->name[0] );
		$term_count = 0;
	    foreach ( $terms as $term ) {
			$first_letter = strtoupper( $term->name[0] );
			if ( $first_letter != $prev_letter ) {
				$alphabet[ $prev_letter ] = $term_count;
				$term_count = 0;
			}
			$term_count++;
			$prev_letter = $first_letter;
		}

		// Dont forget last letter in term list
		$alphabet[ $prev_letter ] = $term_count;	

		$letter_nav = array_keys( $alphabet );
		echo '<div class="term-nav">';
			foreach ( $letter_nav as $letter ) {
				printf('<a href="#term-%s">%s</a>',
					esc_html( $letter ),
					esc_html( $letter )
				);

			}
		echo '</div>';

		//Set up for first time through loop
		$prev_initial = strtoupper( $terms[0]->name[0] );
		echo '<div class="index-group clearfix">';
			printf( '<h2 id="term-%s">%s</h2>',
				esc_html( $prev_initial ),
				esc_html( $prev_initial )
			);
			$count = 0;

			//Loop Through Terms
			foreach ( $terms as $term ) {

				//Find out first letter of term
				$current_initial = strtoupper( $term->name[0] );

				//If its the first time of that letter create a heading and new section
				if ( $current_initial != $prev_initial ) {
					printf( '</div><div class="index-group clearfix"><h2 id="term-%s">%s</h2>',
						esc_html( $current_initial ),
						esc_html( $current_initial )
					);
					$count = 0;
				}

				if( 0 == $count || 0 == $count % $ncols ) {
					$class = $column_class[ $ncols ] . ' first';
				}
				else {
					$class = $column_class[ $ncols ];			
				}

				$post_count = '';
				if( $show_count ) {
					$post_count = '&nbsp;(' . esc_html( $term->count ) . ')';
				}

				printf( '<div class="%s"><a href="%s">%s</a>%s</div>',
					esc_attr( trim( $class ) ),
					esc_url( get_term_link( $term ) ),
					esc_html( ucwords( strtolower( $term->name ) ) ),
					esc_html( $post_count )
				);

				$prev_initial = $current_initial;
				$count++;
			}
		echo '</div>';

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/lang/' );
	}

	/**
	 * Load CSS styles
	 *
	 * @since 1.0.0
	 */
	public function load_styles() {

		// Register plugin CSS
		$css_file = apply_filters( 'cw_taxonomy_index_page_for_genesis_default_css', plugin_dir_url( __FILE__ ) . 'css/style.css' );
		$main_css = 'cw-taxonomy-index-page-for-genesis-style';
		wp_register_style( $main_css, esc_url( $css_file ), array(), '1.0.0', 'screen' );

		// Enqueue main CSS
		wp_enqueue_style( $main_css );

	}


	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Taxonomy Index Page For Genesis Options', 'cw-taxonomy-index-page-for-genesis' ),
			__( 'Taxonomy Index Page For Genesis', 'cw-taxonomy-index-page-for-genesis' ),
			'read',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once( 'views/admin.php' );
	}

	/**
	 * Register the settings.
	 *
	 * @since    1.0.0
	 */
	function register_plugin_settings() {
		register_setting( 'cw-taxonomy-index-page-for-genesis-group', 'taxonomy' );
		register_setting( 'cw-taxonomy-index-page-for-genesis-group', 'ncols' );
		register_setting( 'cw-taxonomy-index-page-for-genesis-group', 'index_page' );
		register_setting( 'cw-taxonomy-index-page-for-genesis-group', 'show_post_count' );
	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		return array_merge(
			array(
				'settings' => sprintf( '<a href="%s">%s</a>',
								esc_url( admin_url( 'options-general.php?page=cw-taxonomy-index-page-for-genesis' ) ),
								__( 'Settings', 'cw-taxonomy-index-page-for-genesis' )
							   )
			),
			$links
		);

	}

}
