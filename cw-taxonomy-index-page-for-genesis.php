<?php
/**
 * Taxonomy Index Page For Genesis
 *
 * @package   CW_Taxonomy_Index_Page_For_Genesis
 * @author Jo Waltham
 * @license GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Taxonomy Index Page For Genesis
 * Plugin URI:  http://www.calliaweb.co.uk/taxonomy-index-page-for-genesis
 * Description: This plugin creates an index of the selected taxonomy terms
 * Version:     1.0.0
 * Author:      Jo Waltham
 * Author URI:  http://www.calliaweb.co.uk/
 * Text Domain: cw-taxonomy-index-page-for-genesis
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Check if Genesis is running, if not deactivate the plugin
add_action( 'init', 'cwtaipfg_init' );
function cwtaipfg_init() {
	if ( 'genesis' !== basename( get_template_directory() ) ) {
		add_action( 'admin_init', 'cwtaipfg_deactivate' );
		add_action( 'admin_notices', 'cwtaipfg_notice' );
		return;
	}
}
function cwtaipfg_deactivate() {
	deactivate_plugins( plugin_basename( __FILE__ ) );
}
function cwtaipfg_notice() {
	echo '<div class="error"><p>';
	echo __( '<strong>Taxonomy Index Page For Genesis</strong> plugin works only with the Genesis Framework. It has been <strong>deactivated</strong>.', 'callia_web_custom_taxonomy_archive_index' );
	echo '</p></div>';
}

require_once( plugin_dir_path( __FILE__ ) . 'class-cw-taxonomy-index-page-for-genesis.php' );

cw_taxonomy_index_page_for_genesis::get_instance();